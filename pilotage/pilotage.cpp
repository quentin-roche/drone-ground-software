#include "pilotage.h"





QString Pilotage::getMessage(){
    return "ord_pitch:"+QString::number(joy_pitch*5)+
           "/ord_roll:"+QString::number(joy_roll*5)+
           "/ord_yaw:"+QString::number(joy_yaw*5)+
           "/ord_pow:"+QString::number((1-joy_thrust)*2000);
}

Pilotage::Pilotage(QObject *parent)
{
    joy_pitch = 0;
    joy_roll = 0;
    joy_yaw = 0;
    joy_thrust = 1;

    Joystick* joystick = Joystick::getInstance();

    connect(joystick,SIGNAL(changeAxisPitch(double)),this,SLOT(joyValuePitch(double)));
    connect(joystick,SIGNAL(changeAxisRoll(double)),this,SLOT(joyValueRoll(double)));
    connect(joystick,SIGNAL(changeAxisYaw(double)),this,SLOT(joyValueYaw(double)));
    connect(joystick,SIGNAL(changeisThrust(double)),this,SLOT(joyValueThrust(double)));

}




void Pilotage::joyValuePitch(double value){

    joy_pitch = value;
}

void Pilotage::joyValueRoll(double value){
    joy_roll = value;
}

void Pilotage::joyValueYaw(double value){
    joy_yaw = value;
}

void Pilotage::joyValueThrust(double value){
    joy_thrust = value;
}
