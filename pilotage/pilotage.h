#ifndef PILOTAGE_H
#define PILOTAGE_H

#include "hardware/joystick.h"
#include <QObject>
#include <QDebug>
#include <QString>



class Pilotage : public QObject
{
    Q_OBJECT


public slots:
    void joyValuePitch(double value);
    void joyValueRoll(double value);
    void joyValueYaw(double value);
    void joyValueThrust(double value);

private :
    double joy_pitch;
    double joy_roll;
    double joy_yaw;
    double joy_thrust;

public :
    Pilotage(QObject *parent = 0);
    QString getMessage();

};

#endif // PILOTAGE_H
