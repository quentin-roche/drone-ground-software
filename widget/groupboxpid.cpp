#include "widget/groupboxpid.h"

GroupBoxPid::GroupBoxPid(const QString & titre)
{
    setTitle(titre);

    QVBoxLayout *layoutPidVitessePitch = new QVBoxLayout;

    layoutPidVitessePitch->addWidget(new QLabel(tr("Propotionnel : ")));
    layoutPidVitessePitch->addWidget(new QLabel(tr("Integrale : ")));
    layoutPidVitessePitch->addWidget(new QLabel(tr("Dérivée : ")));
    layoutPidVitessePitch->addWidget(new QPushButton(tr("Modifier")));
    layoutPidVitessePitch->addStretch(1);


    setLayout(layoutPidVitessePitch);

}
