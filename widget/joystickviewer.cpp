#include "joystickviewer.h"



JoystickViewer::JoystickViewer(){
    setMinimumHeight(32);
    setMinimumWidth(32);



    setSizePolicy(QSizePolicy::MinimumExpanding,QSizePolicy::MinimumExpanding);
}



void JoystickViewer::paintEvent(QPaintEvent *)
{
    QPainter p(this);

    QPen pen;
    pen.setWidth(1);
    pen.setColor(Qt::white);
    p.setPen(pen);

    p.setRenderHint(QPainter::Antialiasing);





    p.drawRect(5, 5,  rect().width()-15,  rect().height()-15);
    p.drawLine(rect().width()-5, 5, rect().width()-5, rect().height()-10);
    p.drawLine(5, rect().height()-5, rect().width()-10, rect().height()-5);



    pen.setWidth(5);
    pen.setColor(Qt::red);
    p.setPen(pen);

    p.drawPoint(5+(rect().width()-15)/2*(X+1),5+(rect().width()-15)/2*(Y+1));
    p.drawPoint(rect().width()-5, (rect().height()-15)/2*(P+1)+5);
    p.drawPoint(5+(rect().width()-15)/2*(Z+1), rect().height()-5);



}

void JoystickViewer::setPitch(double value){
    Y = value;
    this->update();
}
void JoystickViewer::setRoll(double value){
    X = value;
    this->update();
}
void JoystickViewer::setYaw(double value){
    Z = value;
    this->update();
}
void JoystickViewer::setThrust(double value){
    P = value;
    this->update();
}
