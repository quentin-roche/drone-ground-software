#ifndef RETOURVIDEO_H
#define RETOURVIDEO_H

#include <QTimer>
#include <QTime>
#include <Qt5GStreamer/QGst/Pipeline>
#include <Qt5GStreamer/QGst/Ui/VideoWidget>
class RetourVideo : public QGst::Ui::VideoWidget
{
    Q_OBJECT
public:
    RetourVideo(QWidget *parent = 0);
    ~RetourVideo();
    void setUri(const QString & uri);
    QTime position() const;
    void setPosition(const QTime & pos);
    int volume() const;
    QTime length() const;
    QGst::State state() const;
public Q_SLOTS:
    void play();
    void pause();
    void stop();
    void setVolume(int volume);
Q_SIGNALS:
    void positionChanged();
    void stateChanged();
private:
    void onBusMessage(const QGst::MessagePtr & message);
    void handlePipelineStateChange(const QGst::StateChangedMessagePtr & scm);
    QGst::PipelinePtr m_pipeline;
    QTimer m_positionTimer;
};

#endif
