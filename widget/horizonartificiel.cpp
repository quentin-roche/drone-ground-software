#include "widget/horizonartificiel.h"


HorizonArtificiel::HorizonArtificiel(){

    setSizePolicy(QSizePolicy::MinimumExpanding,QSizePolicy::MinimumExpanding);
    pitch = 10*6.28/360;
    roll = 5*6.28/360;
    altitude = 0;
    vitesse = 0;
}

void HorizonArtificiel::setPitch(float val_pitch)
{
    pitch = val_pitch;
    this->update();
}

void HorizonArtificiel::setRoll(float val_roll)
{
    roll = val_roll;
    this->update();
}

void HorizonArtificiel::setVitesse(float val_vitesse)
{
    vitesse = val_vitesse;
    this->update();
}

void HorizonArtificiel::setAltitude(float val_altitude)
{
    altitude = val_altitude;
    this->update();
}



void HorizonArtificiel::paintEvent(QPaintEvent *)
{
    QPainter p(this);

    QPen pen;
    pen.setWidth(2);
    pen.setColor(Qt::white);
    p.setPen(pen);

    p.setRenderHint(QPainter::Antialiasing);

    int taille;
    if (rect().width() > rect().height())
        taille = rect().width()*2;
    else
        taille = rect().height()*2;

    int y_pitch = (-pitch*rect().height())/cos(-roll);

    int ySepareGauche = rect().height()/2 - double(rect().width()/2)*tan(-roll) + y_pitch;
    int ySepareDroite = rect().height()/2 + double(rect().width()/2)*tan(-roll) + y_pitch;

    if (ySepareGauche < 0)
        ySepareGauche = 0;
    if (ySepareGauche > rect().height())
        ySepareGauche = rect().height();

    if (ySepareDroite < 0)
        ySepareDroite = 0;
    if (ySepareDroite > rect().height())
        ySepareDroite = rect().height();


    //Horizon
    QPainterPath pathCiel;
    pathCiel.moveTo (0, 0);
    pathCiel.lineTo(rect().width(), 0);
    pathCiel.lineTo(rect().width(), ySepareDroite);
    pathCiel.lineTo(0, ySepareGauche);
    p.fillPath(pathCiel, QColor(107,141,228));

    QPainterPath pathSol;
    pathSol.moveTo (0, rect().height());
    pathSol.lineTo(rect().width(), rect().height());
    pathSol.lineTo(rect().width(), ySepareDroite);
    pathSol.lineTo(0, ySepareGauche);
    p.fillPath(pathSol, QColor(118,79,38));


    p.drawLine(0,ySepareGauche,rect().width(),ySepareDroite);




    //indicateur pitch
    pen.setColor(QColor(255,255,255));
    pen.setWidth(1);
    p.setPen(pen);

    double long_x = 20*cos(-roll);
    double long_y = 20*sin(-roll);
    double short_x = 5*cos(-roll);
    double short_y = 5*sin(-roll);

    double espace_x = - 5*6.28/360 * rect().height() * sin(-roll);
    double espace_y = 5*6.28/360 * rect().height() * cos(-roll);

    double centre_x = rect().width()/2;
    double centre_y = rect().height()/2 + (-pitch*rect().height())/cos(-roll);

    for(int k = -10; k<=10; k++){
        if (k != 0){
            if (k%2 == 0){
                p.drawLine(centre_x + espace_x*k - long_x,
                           centre_y + espace_y*k - long_y,
                           centre_x + espace_x*k + long_x,
                           centre_y + espace_y*k + long_y);

                p.drawText(centre_x + espace_x*k + long_x+5,
                           centre_y + espace_y*k + long_y+5,
                           QString::number(-k*10)+"°");
            }else{
                p.drawLine(centre_x + espace_x*k - short_x,
                           centre_y + espace_y*k - short_y,
                           centre_x + espace_x*k + short_x,
                           centre_y + espace_y*k + short_y);
            }
        }
    }

    //Altitude et vitesse

    p.drawText(3,15,QString::number(vitesse,'f', 0)+" km/h");
    p.drawText(3,29,QString::number(altitude,'f', 1)+" m");

    //Ligne de vol
    pen.setColor(QColor(242,109,0));
    pen.setWidth(2);
    p.setPen(pen);
    p.drawLine(rect().width()*4/14,rect().height()/2,rect().width()*6/14,rect().height()/2);
    p.drawLine(rect().width()*6/14,rect().height()/2,rect().width()*7/14,rect().height()*16/29);
    p.drawLine(rect().width()*7/14,rect().height()*16/29,rect().width()*8/14,rect().height()/2);
    p.drawLine(rect().width()*8/14,rect().height()/2,rect().width()*10/14,rect().height()/2);
    p.drawPoint(rect().width()/2,rect().height()/2);





}
