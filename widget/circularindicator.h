#ifndef CIRCULARINDICATOR_H
#define CIRCULARINDICATOR_H


#include <QWidget>
#include <QPaintEvent>


#include <QtWidgets>
#include <QtGui>

class CircularIndicator : public QWidget
{
    Q_OBJECT

public:
    CircularIndicator();
    CircularIndicator(QWidget *parent);

signals:

public slots:
    void setValue(float val);

protected:
    void paintEvent(QPaintEvent *);

private:
    double progress;

};

#endif // CIRCULARINDICATOR_H
