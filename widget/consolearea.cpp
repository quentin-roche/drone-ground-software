#include "widget/consolearea.h"

ConsoleArea::ConsoleArea(const QString & titre)
{


    setTitle(titre);

    QVBoxLayout *layout_box = new QVBoxLayout;
    setLayout(layout_box);

    //Setting scrollArea
    QScrollArea *scrollArea = new QScrollArea;
    layout_box->addWidget(scrollArea);
    scrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    scrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    scrollArea->setWidgetResizable(true);



    //Creation du layout vertical
    layout_liste = new QVBoxLayout();



    //Creation du widget conteneur
    QWidget *container_list = new QWidget();
    container_list->setLayout(layout_liste);
    scrollArea->setWidget(container_list);
}

void ConsoleArea::addLine(QString message){
    QLabel *label = new QLabel(message);
    label->setWordWrap(true);
    layout_liste->addWidget(label);
}

