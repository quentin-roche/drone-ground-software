#include "circularindicator.h"


CircularIndicator::CircularIndicator(){
    setMinimumHeight(32);
    setMinimumWidth(32);

    setValue(1);

    setSizePolicy(QSizePolicy::MinimumExpanding,QSizePolicy::MinimumExpanding);
}

void CircularIndicator::setValue(float val)
{
    progress = (double)val/100;
    //yes, it is not very good, the best approach is to
    //create something similar to QProgressBar
    this->update();
}


void CircularIndicator::paintEvent(QPaintEvent *)
{
    QPainter p(this);

    QPen pen;

    p.setRenderHint(QPainter::Antialiasing);

    QRectF rectangle(1.0, 1.0, 30.0, 30.0);
    //to understand these magic numbers, look drawArc method in Qt doc
    int startAngle = -90 * 16;
    int spanAngle = progress * 360 * 16;


    pen.setWidth(2);
    pen.setColor(QColor(30,30,30));
    p.setPen(pen);
    p.drawArc(rectangle, 0, 360*16);
    p.drawText(13,20,"1");


    pen.setWidth(2);
    pen.setColor(Qt::red);
    p.setPen(pen);
    p.drawArc(rectangle, startAngle, spanAngle);





    //p.drawText(rectangle,Qt::AlignCenter,nom);
}
