#ifndef JOYSTICKVIEWER_H
#define JOYSTICKVIEWER_H



#include <QWidget>
#include <QPaintEvent>


#include <QtWidgets>
#include <QtGui>

class JoystickViewer : public QWidget
{

    Q_OBJECT
public:
    JoystickViewer();
    JoystickViewer(QWidget *parent);

signals:

public slots:
    void setPitch(double value);
    void setRoll(double value);
    void setYaw(double value);
    void setThrust(double value);


protected:
    void paintEvent(QPaintEvent *);

private:
    double X = 0;
    double Y = 0;
    double Z = 0;
    double P = 1;
};

#endif // JOYSTICKVIEWER_H
