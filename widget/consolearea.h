#ifndef CONSOLEAREA_H
#define CONSOLEAREA_H



#include <QtWidgets>
#include <QtGui>


class ConsoleArea : public QGroupBox
{
     Q_OBJECT
private:
    QVBoxLayout *layout_liste;
public:
    ConsoleArea(const QString & titre);

public slots:
    void addLine(QString message);
};

#endif // CONSOLEAREA_H
