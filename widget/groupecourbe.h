#ifndef GROUPECOURBE_H
#define GROUPECOURBE_H


#include <QtWidgets>
#include <QtGui>

class GroupeCourbe : public QGroupBox
{
public:
    GroupeCourbe(const QString & titre);
private:
    int nbPoints = 5;
    double xDebut = -1;
    double xFin = 1;

    QList<float> xCourbe;
    QList<float> yCourbe;

    QTableView *tyty;

};

#endif // GROUPECOURBE_H
