#ifndef HORIZONARTIFICIEL_H
#define HORIZONARTIFICIEL_H


#include <QWidget>
#include <QPaintEvent>


#include <QtWidgets>
#include <QtGui>

#include <math.h>

class HorizonArtificiel : public QWidget
{
    Q_OBJECT

public:
    HorizonArtificiel();
    HorizonArtificiel(QWidget *parent);

signals:

public slots:
    void setPitch(float pitch);
    void setRoll(float roll);
    void setVitesse(float vitesse);
    void setAltitude(float altitude);

protected:
    void paintEvent(QPaintEvent *);

private:
    double pitch;
    double roll;
    double vitesse;
    double altitude;

};


#endif // HORIZONARTIFICIEL_H
