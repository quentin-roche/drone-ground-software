# This is a qmake project file, provided as an example on how to use qmake with QtGStreamer.


QT += core gui

TEMPLATE = app
TARGET = player

# produce nice compilation output
CONFIG += silent

# Tell qmake to use pkg-config to find QtGStreamer.
CONFIG += link_pkgconfig




# Recommended if you are using g++ 4.5 or later. Must be removed for other compilers.
#QMAKE_CXXFLAGS += -std=c++0x

# Recommended, to avoid possible issues with the "emit" keyword
# You can otherwise also define QT_NO_EMIT, but notice that this is not a documented Qt macro.
DEFINES += QT_NO_KEYWORDS

# Input
HEADERS += mediaapp.h player.h
SOURCES += main.cpp mediaapp.cpp player.cpp



CONFIG += link_pkgconfig
LIBS += -lQt5GStreamer-1.0 -lQt5GStreamerUi-1.0


