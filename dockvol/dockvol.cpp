#include "dockvol.h"
#include "widget/joystickviewer.h"
#include "widget/horizonartificiel.h"
#include "serveur/mytcpserver.h"
#include "serveur/receptionconnecteur.h"


DockVol::DockVol()
{
    //Creation du dock

    setMinimumHeight(190);
    setMaximumHeight(minimumHeight());

    setAllowedAreas(Qt::BottomDockWidgetArea);
    setFeatures(QDockWidget::NoDockWidgetFeatures);


    //Cration du layout horizontal
    QHBoxLayout *layout_vol = new QHBoxLayout();

    //Creation du widget conteneur
    QWidget *container_vol = new QWidget();
    container_vol->setLayout(layout_vol);
    setWidget(container_vol);


    MyTcpServer *serveur = MyTcpServer::getInstance();


    //Creation du groupe motorisation
    QGroupBox *groupMotorisation = new QGroupBox(tr("Motorisation"));
    groupMotorisation->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
    QGridLayout* layoutMotrisation = new QGridLayout(groupMotorisation);

    CircularIndicator *circularIndicator0 = new CircularIndicator();
    CircularIndicator *circularIndicator1 = new CircularIndicator();
    CircularIndicator *circularIndicator2 = new CircularIndicator();
    CircularIndicator *circularIndicator3 = new CircularIndicator();
    CircularIndicator *circularIndicator4 = new CircularIndicator();
    CircularIndicator *circularIndicator5 = new CircularIndicator();
    connect(new ReceptionConnecteur("mot_pow_0"), SIGNAL(signalValue(float)), circularIndicator0, SLOT(setValue(float)));
    connect(new ReceptionConnecteur("mot_pow_1"), SIGNAL(signalValue(float)), circularIndicator1, SLOT(setValue(float)));
    connect(new ReceptionConnecteur("mot_pow_2"), SIGNAL(signalValue(float)), circularIndicator2, SLOT(setValue(float)));
    connect(new ReceptionConnecteur("mot_pow_3"), SIGNAL(signalValue(float)), circularIndicator3, SLOT(setValue(float)));
    connect(new ReceptionConnecteur("mot_pow_4"), SIGNAL(signalValue(float)), circularIndicator4, SLOT(setValue(float)));
    connect(new ReceptionConnecteur("mot_pow_5"), SIGNAL(signalValue(float)), circularIndicator5, SLOT(setValue(float)));
    layoutMotrisation->addWidget(circularIndicator0, 0, 0);
    layoutMotrisation->addWidget(circularIndicator1, 1, 0);
    layoutMotrisation->addWidget(circularIndicator2, 2, 0);
    layoutMotrisation->addWidget(circularIndicator3, 0, 1);
    layoutMotrisation->addWidget(circularIndicator4, 1, 1);
    layoutMotrisation->addWidget(circularIndicator5, 2, 1);
    layout_vol->addWidget(groupMotorisation);

    //Creation de la console
    ConsoleArea* console = new ConsoleArea("Console");
    console->setMinimumWidth(350);
    layout_vol->addWidget(console);
    QObject::connect(serveur, SIGNAL(messagePrincipale(QString)), console, SLOT(addLine(QString)));

    //Creation de l'horizon artificiel
    HorizonArtificiel *horizonArtificiel = new HorizonArtificiel();
    layout_vol->addWidget(horizonArtificiel);
    horizonArtificiel->setFixedSize(190,150);
    connect(new ReceptionConnecteur("real_pitch"), SIGNAL(signalValue(float)), horizonArtificiel, SLOT(setPitch(float)));
    connect(new ReceptionConnecteur("real_roll"), SIGNAL(signalValue(float)), horizonArtificiel, SLOT(setRoll(float)));
    connect(new ReceptionConnecteur("real_speed"), SIGNAL(signalValue(float)), horizonArtificiel, SLOT(setVitesse(float)));
    connect(new ReceptionConnecteur("real_altitude"), SIGNAL(signalValue(float)), horizonArtificiel, SLOT(setAltitude(float)));





    layout_vol->addStretch(1);




}
