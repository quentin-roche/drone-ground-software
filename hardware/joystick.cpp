#include "joystick.h"



Joystick *Joystick::getInstance(){
    return Joystick::instance;
}

Joystick::Joystick(QObject *parent)
    : QObject(parent)
    , m_gamepad(0)
{
    gamepads = QGamepadManager::instance()->connectedGamepads();
    if (gamepads.isEmpty()) {
        return;
    }



    m_gamepad = new QGamepad(*gamepads.begin(), this);


    connect(m_gamepad, SIGNAL(axisLeftXChanged(double)), this, SLOT(axisLeftXChange(double)));
    connect(m_gamepad, SIGNAL(axisLeftYChanged(double)), this, SLOT(axisLeftYChange(double)));
    connect(m_gamepad, SIGNAL(axisRightXChanged(double)), this, SLOT(axisRightXChange(double)));
    connect(m_gamepad, SIGNAL(axisRightYChanged(double)), this, SLOT(axisRightYChange(double)));


    connect(m_gamepad, &QGamepad::buttonAChanged, this, [](bool pressed){
        qDebug() << "Button A" << pressed;
    });
    connect(m_gamepad, &QGamepad::buttonBChanged, this, [](bool pressed){
        qDebug() << "Button B" << pressed;
    });
    connect(m_gamepad, &QGamepad::buttonXChanged, this, [](bool pressed){
        qDebug() << "Button X" << pressed;
    });
    connect(m_gamepad, &QGamepad::buttonYChanged, this, [](bool pressed){
        qDebug() << "Button Y" << pressed;
    });
    connect(m_gamepad, &QGamepad::buttonL1Changed, this, [](bool pressed){
        qDebug() << "Button L1" << pressed;
    });
    connect(m_gamepad, &QGamepad::buttonR1Changed, this, [](bool pressed){
        qDebug() << "Button R1" << pressed;
    });
    connect(m_gamepad, &QGamepad::buttonL2Changed, this, [](double value){
        qDebug() << "Button L2: " << value;
    });
    connect(m_gamepad, &QGamepad::buttonR2Changed, this, [](double value){
        qDebug() << "Button R2: " << value;
    });
    connect(m_gamepad, &QGamepad::buttonSelectChanged, this, [](bool pressed){
        qDebug() << "Button Select" << pressed;
    });
    connect(m_gamepad, &QGamepad::buttonStartChanged, this, [](bool pressed){
        qDebug() << "Button Start" << pressed;
    });
    connect(m_gamepad, &QGamepad::buttonGuideChanged, this, [](bool pressed){
        qDebug() << "Button Guide" << pressed;
    });

    Joystick::instance = this;
}

Joystick *Joystick::instance;


Joystick::~Joystick()
{
    delete m_gamepad;
}


void Joystick::setAxisPitch(){
    QGamepadManager::instance()->configureAxis(*gamepads.begin(), QGamepadManager::AxisLeftY);
}

void Joystick::setAxisRoll(){
    QGamepadManager::instance()->configureAxis(*gamepads.begin(), QGamepadManager::AxisLeftX);
}
void Joystick::setAxisYaw(){
    QGamepadManager::instance()->configureAxis(*gamepads.begin(), QGamepadManager::AxisRightX);
}
void Joystick::setAxisThrust(){
    QGamepadManager::instance()->configureAxis(*gamepads.begin(), QGamepadManager::AxisRightY);
}

void Joystick::axisLeftYChange(double value){
    emit changeAxisPitch(value);
}

void Joystick::axisLeftXChange(double value){
    emit changeAxisRoll(value);
}

void Joystick::axisRightYChange(double value){
    emit changeisThrust(value);
}

void Joystick::axisRightXChange(double value){
    emit changeAxisYaw(value);
}


