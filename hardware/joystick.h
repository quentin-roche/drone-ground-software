#ifndef JOYSTICK_H
#define JOYSTICK_H



#include <QtCore/QObject>
#include <QtCore/QTimer>
#include <QGamepadManager>
#include <QGamepad>
#include <QGamepadKeyNavigation>
#include <QDebug>

class QGamepad;

class Joystick : public QObject
{
    Q_OBJECT
public:
    Joystick(QObject *parent = 0);
    ~Joystick();

    static Joystick *getInstance();



public slots:
    void setAxisPitch();
    void setAxisRoll();
    void setAxisYaw();
    void setAxisThrust();

    void axisLeftXChange(double);
    void axisLeftYChange(double);
    void axisRightXChange(double);
    void axisRightYChange(double);



signals:
    void changeAxisPitch(double);
    void changeAxisRoll(double);
    void changeAxisYaw(double);
    void changeisThrust(double);

private:
    static Joystick *instance;
    QGamepad *m_gamepad;
    QList<int> gamepads;
};

#endif // JOYSTICK_H

