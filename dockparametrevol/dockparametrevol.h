#ifndef DOCKPARAMETREVOL_H
#define DOCKPARAMETREVOL_H




#include <QtWidgets>
#include <QtGui>


#include "dockparametrevol/tabasservissement.h"
#include "dockparametrevol/tabjoystick.h"
#include "dockparametrevol/tabpilotageangle.h"

class DockParametreVol : public QDockWidget
{
public:
    DockParametreVol();
};

#endif // DOCK_PARAMETRE_VOL_H
