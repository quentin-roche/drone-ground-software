#include "tabpilotageangle.h"

TabPilotageAngle::TabPilotageAngle()
{

    setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    setWidgetResizable(true);


    //Creation du layout vertical
    QVBoxLayout *layout = new QVBoxLayout();

    //Creation du widget conteneur
    QWidget *container_asservissement = new QWidget();
    container_asservissement->setLayout(layout);
    setWidget(container_asservissement);


    GroupeCourbe *groupeCourbePitch = new GroupeCourbe(tr("Courbe tangage"));
    layout->addWidget(groupeCourbePitch);


    GroupeCourbe *groupeCourbeRoll = new GroupeCourbe(tr("Courbe roulis"));

    layout->addWidget(groupeCourbeRoll);


    GroupeCourbe *groupeCourbeYaw = new GroupeCourbe(tr("Courbe lacet"));
    layout->addWidget(groupeCourbeYaw);


    layout->addStretch(1);
}
