#include "tabasservissement.h"

TabAsservissement::TabAsservissement()
{


    setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    setWidgetResizable(true);


    //Creation du layout vertical
    QVBoxLayout *layout_asservissement = new QVBoxLayout();

    //Creation du widget conteneur
    QWidget *container_asservissement = new QWidget();
    container_asservissement->setLayout(layout_asservissement);
    setWidget(container_asservissement);


    //Creation paramètre

    layout_asservissement->addWidget(new GroupBoxPid(tr("PID vitesse pitch :")));
    layout_asservissement->addWidget(new GroupBoxPid(tr("PID vitesse roll :")));
    layout_asservissement->addWidget(new GroupBoxPid(tr("PID vitesse yaw :")));

    layout_asservissement->addSpacerItem(new QSpacerItem(1,50));

    layout_asservissement->addWidget(new GroupBoxPid(tr("PID angle pitch :")));
    layout_asservissement->addWidget(new GroupBoxPid(tr("PID angle roll :")));
    layout_asservissement->addWidget(new GroupBoxPid(tr("PID angle yaw :")));

    layout_asservissement->addSpacerItem(new QSpacerItem(1,50));

    layout_asservissement->addWidget(new GroupBoxPid(tr("PID altitude :")));





    layout_asservissement->addStretch(1);

}
