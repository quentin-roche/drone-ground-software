#include "dockparametrevol.h"

DockParametreVol::DockParametreVol()
{
    //Creation du dock

    setWindowTitle(tr("Paramètres de vol"));
    setMaximumWidth(700);
    setMinimumWidth(250);
    setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
    setFeatures(QDockWidget::DockWidgetVerticalTitleBar | QDockWidget::DockWidgetMovable);

    //Creation du tab Widget
    QTabWidget *tab_vol= new QTabWidget(this);
    tab_vol->setTabPosition(QTabWidget::West);
    tab_vol->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
    setWidget(tab_vol);

    //Creation de la scroll area
    TabAsservissement *tabAsservissement = new TabAsservissement();
    tab_vol->addTab(tabAsservissement, "Asservissements");


    TabJoystick *tabJoystick = new TabJoystick();
    tab_vol->addTab(tabJoystick, "Joystick");


    TabPilotageAngle *tabPilotageAngle = new TabPilotageAngle();
    tab_vol->addTab(tabPilotageAngle, "Pilotage Angle");






}
