#include "tabjoystick.h"

TabJoystick::TabJoystick()
{


    setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    setWidgetResizable(true);


    //Creation du layout vertical
    QVBoxLayout *layout = new QVBoxLayout();

    //Creation du widget conteneur
    QWidget *container = new QWidget();
    container->setLayout(layout);
    setWidget(container);



    JoystickViewer *joystickViewer = new JoystickViewer();
    layout->addWidget(joystickViewer);
    joystickViewer->setFixedSize(100,100);
    layout->setAlignment(joystickViewer, Qt::AlignHCenter);

    QGroupBox *groupeSetAxis = new QGroupBox(tr("Lier axes et boutons"));
    layout->addWidget(groupeSetAxis);
    QGridLayout* layoutSetAxis = new QGridLayout(groupeSetAxis);


    QPushButton *boutonSetAxisPitch = new QPushButton(tr("Tangage"));
    QPushButton *boutonSetAxisRoll = new QPushButton(tr("Roulis"));
    QPushButton *boutonSetAxisYaw = new QPushButton(tr("Lacet"));
    QPushButton *boutonSetAxisThrust = new QPushButton(tr("Poussée"));

    QLabel *explication = new QLabel(tr("Cette partie permet de régler les axes et boutons de votre joystick. Pour régler un axe (ou un bouton) il suffit de le sélectionner puis de l'utiliser pour permettre sa détection."));
    explication->setWordWrap(true);
    layoutSetAxis->addWidget(explication);
    layoutSetAxis->addWidget(boutonSetAxisPitch);
    layoutSetAxis->addWidget(boutonSetAxisRoll);
    layoutSetAxis->addWidget(boutonSetAxisYaw);
    layoutSetAxis->addWidget(boutonSetAxisThrust);

    Joystick *joystick = Joystick::getInstance();

    connect(boutonSetAxisPitch,SIGNAL(pressed()),joystick,SLOT(setAxisPitch()));
    connect(boutonSetAxisRoll,SIGNAL(pressed()),joystick,SLOT(setAxisRoll()));
    connect(boutonSetAxisYaw,SIGNAL(pressed()),joystick,SLOT(setAxisYaw()));
    connect(boutonSetAxisThrust,SIGNAL(pressed()),joystick,SLOT(setAxisThrust()));

    connect(joystick,SIGNAL(changeAxisPitch(double)),joystickViewer,SLOT(setPitch(double)));
    connect(joystick,SIGNAL(changeAxisRoll(double)),joystickViewer,SLOT(setRoll(double)));
    connect(joystick,SIGNAL(changeAxisYaw(double)),joystickViewer,SLOT(setYaw(double)));
    connect(joystick,SIGNAL(changeisThrust(double)),joystickViewer,SLOT(setThrust(double)));





    layout->addStretch(1);

}
