#ifndef MYTCPSERVER_H
#define MYTCPSERVER_H

#include <QObject>
#include <QTcpSocket>
#include <QTcpServer>
#include <QDebug>
#include <QDataStream>

#include "widget/consolearea.h"
#include "pilotage/pilotage.h"

class MyTcpServer : public QObject
{
    Q_OBJECT
public:
    explicit MyTcpServer(QObject *parent = 0);
    static MyTcpServer *getInstance();
    static void setInstance(MyTcpServer *instance);

    void setPilotage(Pilotage *pilotage);

signals:
    void messagePrincipale(QString message);
    void nevStringParametre(QString message);

public slots:
    void newConnection();
    void deconnexionClient();
    void donneesRecues();

private:
    static MyTcpServer* instance;
    void envoyerATous(QString message);

    Pilotage *pilotage;

    QTcpServer *server;
    QList<QTcpSocket *> clients;
    quint16 tailleMessage;
};

#endif // MYTCPSERVER_H
