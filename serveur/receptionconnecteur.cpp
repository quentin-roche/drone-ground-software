#include "receptionconnecteur.h"

ReceptionConnecteur::ReceptionConnecteur(const QString k)
{
    key = k;

    MyTcpServer *serveur = MyTcpServer::getInstance();
    connect(serveur, SIGNAL(nevStringParametre(QString)), this, SLOT(newValues(QString)));

}


void ReceptionConnecteur::newValues(QString str){
    try{
        QStringList separ = str.split(":");

        if(separ[0].compare(key)){
            return;
        }else{
            emit signalValue(separ[1].toFloat());
        }
    }catch(...){

    }
}
