#ifndef SERVEUR_H
#define SERVEUR_H


#include <QtWidgets>
#include <QtNetwork>

#include "widget/consolearea.h"
#include <QDebug>

#include "tcpserver.h"

class Serveur : public QObject
{
    Q_OBJECT

    public:

        explicit Serveur(QObject *parent = 0);

        static Serveur* getInstance( );
        void envoyerATous(const QString &message);
        void setMainConsole(ConsoleArea* _mainConsole);


        void report(QString message);


    public slots:
        void nouvelleConnexion();
        void donneesRecues();
        void deconnexionClient();
        void rapport();

    private:
        static bool instanceFlag;
        static Serveur* instance;
        ~Serveur();

        bool mainConsoleSet = false;
        ConsoleArea *mainConsole;



        QTcpServer *serveurTcp;
        QList<QTcpSocket *> clients;
        quint16 tailleMessage;

        QTimer *timer;

};




#endif // SERVEUR_H
