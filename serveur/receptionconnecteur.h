#ifndef RECEPTIONCONNECTEUR_H
#define RECEPTIONCONNECTEUR_H

#include <QObject>
#include <QTcpSocket>
#include <QDebug>
#include "mytcpserver.h"

class ReceptionConnecteur : public QObject
{
    Q_OBJECT
public:
    ReceptionConnecteur(const QString key);


public slots:
    void newValues(QString);

signals:
    void signalValue(float value);
private:
    QString key;
};

#endif // RECEPTIONCONNECTEUR_H
