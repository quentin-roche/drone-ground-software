#include "mytcpserver.h"
#include <qdebug.h>


MyTcpServer *MyTcpServer::getInstance(){
    return MyTcpServer::instance;
}
void MyTcpServer::setInstance(MyTcpServer *inst){
    MyTcpServer::instance = inst;
}

MyTcpServer::MyTcpServer(QObject *parent) :
    QObject(parent)
{
    pilotage = new Pilotage(this);
    server = new QTcpServer(this);

    // whenever a user connects, it will emit signal
    connect(server, SIGNAL(newConnection()),
            this, SLOT(newConnection()));

    if(!server->listen(QHostAddress::Any, 50885))
    {
        qDebug() << "Server could not start : " << server->errorString();
    }
    else
    {
        qDebug() << "Server started!";
    }



    tailleMessage = 0;
    MyTcpServer::instance = this;
}

MyTcpServer *MyTcpServer::instance;

void MyTcpServer::newConnection()
{
    emit messagePrincipale("Nouvelle connection.");

    qDebug() << "New connection";
    //envoyerATous(tr("<em>Un nouveau client vient de se connecter</em>"));


    QTcpSocket *nouveauClient = server->nextPendingConnection();

    clients << nouveauClient;



    connect(nouveauClient, SIGNAL(readyRead()), this, SLOT(donneesRecues()));

    connect(nouveauClient, SIGNAL(disconnected()), this, SLOT(deconnexionClient()));

}

void MyTcpServer::donneesRecues()

{



    // 1 : on reçoit un paquet (ou un sous-paquet) d'un des clients


    // On détermine quel client envoie le message (recherche du QTcpSocket du client)

    QTcpSocket *socket = qobject_cast<QTcpSocket *>(sender());

    if (socket == 0) // Si par hasard on n'a pas trouvé le client à l'origine du signal, on arrête la méthode

        return;

    QString message = QString(socket->readAll());



    try{
        QStringList separ = message.split("/");

        for(int k=0; k<separ.length(); k++){
            emit nevStringParametre(separ[k]);
        }
    }catch(...){

    }

    message = pilotage->getMessage();

    envoyerATous(message);

}

void MyTcpServer::deconnexionClient()

{
    // On détermine quel client se déconnecte


    QTcpSocket *socket = qobject_cast<QTcpSocket *>(sender());

    if (socket == 0) // Si par hasard on n'a pas trouvé le client à l'origine du signal, on arrête la méthode

        return;


    clients.removeOne(socket);


    socket->deleteLater();
    emit messagePrincipale(tr("Déconnexion."));


}


void MyTcpServer::envoyerATous(QString message)

{



    // Envoi du paquet préparé à tous les clients connectés au serveur

    for (int i = 0; i < clients.size(); i++)

    {

        clients[i]->write(message.toUtf8());

    }


}




void MyTcpServer::setPilotage(Pilotage *pilot){
    pilotage = pilot;
}

