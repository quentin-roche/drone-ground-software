#-------------------------------------------------
#
# Project created by QtCreator 2017-05-01T14:53:31
#
#-------------------------------------------------

QT += core gui
QT += widgets network
QT += gamepad


greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Controleur_vol
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += main.cpp \
    mainwindow.cpp \
    dockparametrevol/tabasservissement.cpp \
    dockvol/dockvol.cpp \
    widget/boussole.cpp \
    widget/circularindicator.cpp \
    widget/consolearea.cpp \
    widget/groupboxpid.cpp \
    widget/horizonartificiel.cpp \
    dockparametrevol/dockparametrevol.cpp \
    widget/groupecourbe.cpp \
    widget/joystickviewer.cpp \
    dockparametrevol/tabjoystick.cpp \
    serveur/mytcpserver.cpp \
    serveur/receptionconnecteur.cpp \
    hardware/joystick.cpp \
    dockparametrevol/tabpilotageangle.cpp \
    groupboxcourbe.cpp \
    pilotage/pilotage.cpp





FORMS    += mainwindow.ui



HEADERS += \
    mainwindow.h \
    dockparametrevol/dockparametrevol.h \
    dockparametrevol/tabasservissement.h \
    dockvol/dockvol.h \
    widget/boussole.h \
    widget/circularindicator.h \
    widget/consolearea.h \
    widget/groupboxpid.h \
    widget/horizonartificiel.h \
    widget/groupecourbe.h \
    widget/joystickviewer.h \
    dockparametrevol/tabjoystick.h \
    serveur/mytcpserver.h \
    serveur/receptionconnecteur.h \
    hardware/joystick.h \
    dockparametrevol/tabpilotageangle.h \
    groupboxcourbe.h \
    pilotage/pilotage.h





DISTFILES += \
    QGst/QtGStreamer-1.0.pc.in \
    QGst/QtGStreamerQuick-1.0.pc.in \
    QGst/QtGStreamerUi-1.0.pc.in \
    QGst/QtGStreamerUtils-1.0.pc.in \
    QGst/CMakeLists.txt








